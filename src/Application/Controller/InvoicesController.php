<?php

namespace App\Controller;

use App\Service\RequestParser;
use Domain\Common\Contracts\PresenterInterface;
use Domain\Invoice\AddCompany\AddCompanyErrorResponse;
use Domain\Invoice\AddCompany\AddCompanyRequest;
use Domain\Invoice\AddCompany\AddCompanyResponse;
use Domain\Invoice\AddCompany\AddCompanyUseCase;
use Domain\Invoice\Contracts\CreateInvoiceUseCaseInterface;
use Domain\Invoice\Contracts\InvoiceGatewayInterface;
use Domain\Invoice\Create\CreateErrorResponse;
use Domain\Invoice\Create\CreateRequest;
use Domain\Invoice\Create\CreateResponse;
use Infrastructure\Model\CompanyModel;
use Infrastructure\Model\InvoiceModel;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InvoicesController extends AbstractController
{
    /**
     * Check api status
     * @Route("/status", name="healthcheck", methods={"GET"})
     * @return JsonResponse
     */
    public function status(): JsonResponse
    {
        return $this->json(['check' => 'ok']);
    }

    /**
     * @Route("/addCompany", name="addCompany", methods={"POST"})
     * @return JsonResponse
     */
    public function addCompany(Request $request, RequestParser $requestParser, AddCompanyUseCase $addCompanyUseCase, PresenterInterface $presenter): JsonResponse
    {
        // Parse the request content to model class
        $companyModel = $requestParser->deserialize($request->getContent(), CompanyModel::class);
        try {
            $companyId = $addCompanyUseCase->handleRequest(
                new AddCompanyRequest($companyModel)
            );
            // present the response by the presenter
            return $presenter->present(new AddCompanyResponse($companyId));
        } catch (\Exception $e) {
            return $presenter->presentError(new AddCompanyErrorResponse($e->getMessage(), $e->getCode()));
        }
    }

    /**
     * @Route("/addInvoice", name="addInvoice", methods={"POST"})
     * @return JsonResponse
     */
    public function addInvoice(Request $request, RequestParser $requestParser, CreateInvoiceUseCaseInterface $createInvoiceUseCase, PresenterInterface $presenter): JsonResponse
    {
        // Parse the request content to model class
        $invoiceModel = $requestParser->deserialize($request->getContent(), InvoiceModel::class);
        try {
            $invoiceId = $createInvoiceUseCase->handleRequest(
                new CreateRequest($invoiceModel)
            );
            // present the response by the presenter
            return $presenter->present(new CreateResponse($invoiceId));
        } catch (\Exception $e) {
            return $presenter->presentError(new CreateErrorResponse($e->getMessage(), $e->getCode()));
        }

    }

    /**
     * @Route("/mark-as-paid/{id}", name="markAsPaid", methods={"PATCH"})
     * @return JsonResponse
     */
    public function markInvoiceAsPaid(int $id, InvoiceGatewayInterface $invoiceGateway): JsonResponse
    {
        $invoiceGateway->markInvoiceAsPaid($id);
        return new JsonResponse([], Response::HTTP_NO_CONTENT);
    }
}