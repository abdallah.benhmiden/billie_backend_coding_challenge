<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class RequestParser
{
    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }


    public function deserialize($content, $class)
    {
        $content = strip_tags($content);
        $invoiceModel =  $this->serializer->deserialize(
            $content,
            $class,
            'json'
        );
        if($this->serializer->serialize($invoiceModel, 'json')) {
            return $invoiceModel;
        }
        throw new \InvalidArgumentException('Invalid Json', Response::HTTP_BAD_REQUEST);
    }


}
