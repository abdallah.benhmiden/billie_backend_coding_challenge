<?php

namespace Infrastructure\Persistence\Entity;

use Infrastructure\Persistence\Repository\CompanyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CompanyRepository::class)
 */
class Company
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $representativeName;
    /**
     * @ORM\Column(type="string", length=20)
     */
    private $phoneNumber;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $adress;
    /**
     * @ORM\Column(type="string", length=50)
     */
    private $type;


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Company
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getRepresentativeName()
    {
        return $this->representativeName;
    }

    /**
     * @param string $representativeName
     * @return Company
     */
    public function setRepresentativeName(string $representativeName)
    {
        $this->representativeName = $representativeName;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     * @return Company
     */
    public function setPhoneNumber(string $phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * @param string $adress
     * @return Company
     */
    public function setAdress(string $adress)
    {
        $this->adress = $adress;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return Company
     */
    public function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
         return $this->name;
    }

}
