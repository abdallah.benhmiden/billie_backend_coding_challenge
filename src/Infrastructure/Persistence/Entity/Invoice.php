<?php

namespace Infrastructure\Persistence\Entity;

use Infrastructure\Persistence\Repository\InvoiceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InvoiceRepository::class)
 */
class Invoice
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;
    /**
     * @ORM\Column(type="datetime")
     */
    private $creationDate;
    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastModificationDate;
    /**
     * @ORM\Column(type="float")
     */
    private $amount;
    /**
    * @ORM\Column(type="integer")
    */
    private $quantity;
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $serviceName;
    /**
     * @ORM\ManyToOne(targetEntity="Infrastructure\Persistence\Entity\Company")
     */
    private $buyer;

    /**
     * @ORM\ManyToOne(targetEntity="Infrastructure\Persistence\Entity\Company")
     */
    private $seller;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $paid = false;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }

    /**
     * @param mixed $creationDate
     * @return Invoice
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastModificationDate()
    {
        return $this->lastModificationDate;
    }

    /**
     * @param mixed $lastModificationDate
     * @return Invoice
     */
    public function setLastModificationDate($lastModificationDate)
    {
        $this->lastModificationDate = $lastModificationDate;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPaid(): bool
    {
        return $this->paid;
    }

    /**
     * @param bool $paid
     * @return Invoice
     */
    public function setPaid(bool $paid): Invoice
    {
        $this->paid = $paid;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return Invoice
     */
    public function setAmount(float $amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return integer
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param integer $quantity
     * @return Invoice
     */
    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return string
     */
    public function getServiceName()
    {
        return $this->serviceName;
    }

    /**
     * @param string $serviceName
     * @return Invoice
     */
    public function setServiceName(string $serviceName)
    {
        $this->serviceName = $serviceName;
        return $this;
    }

    /**
     * @return Company
     */
    public function getBuyer()
    {
        return $this->buyer;
    }

    /**
     * @param Company $buyer
     * @return Invoice
     */
    public function setBuyer(Company $buyer)
    {
        $this->buyer = $buyer;
        return $this;
    }

    /**
     * @return Company
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * @param Company $seller
     * @return Invoice
     */
    public function setSeller(Company $seller)
    {
        $this->seller = $seller;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateModificationDate() {
        $this->setLastModificationDate(new \DateTime());
    }

    /**
     * @return mixed
     */
    public function __toString()
    {
        $format = "InvoiceModel (id: %s, buyer: %s, seller: %s)\n";
        return sprintf($format, $this->id, $this->buyer, $this->seller);
    }

}
