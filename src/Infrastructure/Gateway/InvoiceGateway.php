<?php

namespace Infrastructure\Gateway;

use Domain\Common\Exception\InvoiceNotFoundException;
use Domain\Invoice\Contracts\InvoiceGatewayInterface;
use Infrastructure\Model\InvoiceModel;
use Infrastructure\Persistence\Entity\Company;
use Infrastructure\Persistence\Entity\Invoice;
use Infrastructure\Persistence\Repository\CompanyRepository;
use Infrastructure\Persistence\Repository\InvoiceRepository;

class InvoiceGateway implements InvoiceGatewayInterface
{


    private InvoiceRepository $invoiceRepository;
    private CompanyRepository $companyRepository;

    /**
     * @param InvoiceRepository $invoiceRepository
     */
    public function __construct(InvoiceRepository $invoiceRepository, CompanyRepository $companyRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
        $this->companyRepository= $companyRepository;
    }

    /**
     * @param int $id
     * @return Invoice|null
     * @throws InvoiceNotFoundException
     */
    public function findInvoiceById(int $id): ?Invoice
    {
        $invoice = $this->invoiceRepository->find($id);
        if(is_null($invoice)){
            throw new InvoiceNotFoundException('Invoicewith id: '.$id. ' is not found');
        }
        return $invoice;

    }

    /**
     * @param InvoiceModel $invoice
     * @return int|null
     */
    public function addInvoice(InvoiceModel $invoiceModel, Company $buyer, Company $seller): ?int
    {

        $invoice = new Invoice();
        $invoice->setAmount($invoiceModel->getAmount())
            ->setBuyer($buyer)
            ->setSeller($seller)
            ->setPaid($invoiceModel->isPaid())
            ->setQuantity($invoiceModel->getQuantity())
            ->setServiceName($invoiceModel->getServiceName())
            ->setCreationDate(new \DateTime());
        return $this->invoiceRepository->add($invoice, true);

    }

    /**
     * @param string $id
     * @throws InvoiceNotFoundException
     */
    public function markInvoiceAsPaid(string $id)
    {
        $invoice = $this->invoiceRepository->find($id);
        if(is_null($invoice)){
            throw new InvoiceNotFoundException('Invoice with id: '.$id . ' is not found');
        }
        $invoice->setPaid(true);
        $this->invoiceRepository->update($invoice);

    }

    /**
     * @param Company $company
     * @return int
     */
    public function countInvoiceByCompany(Company $company): int
    {
       return $this->invoiceRepository->countInvoiceByCompany($company->getId());
    }
}