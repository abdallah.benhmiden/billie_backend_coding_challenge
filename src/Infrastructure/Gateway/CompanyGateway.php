<?php

namespace Infrastructure\Gateway;

use Domain\Common\Exception\CompanyNotFoundException;
use Domain\Common\Exception\InvoiceNotFoundException;
use Domain\Invoice\Contracts\CompanyGatewayInterface;
use Domain\Invoice\Contracts\InvoiceGatewayInterface;
use Infrastructure\Model\CompanyModel;
use Infrastructure\Model\InvoiceModel;
use Infrastructure\Persistence\Entity\Company;
use Infrastructure\Persistence\Entity\Invoice;
use Infrastructure\Persistence\Repository\CompanyRepository;
use Infrastructure\Persistence\Repository\InvoiceRepository;

class CompanyGateway implements CompanyGatewayInterface
{


    private InvoiceRepository $invoiceRepository;
    private CompanyRepository $companyRepository;

    /**
     * @param InvoiceRepository $invoiceRepository
     */
    public function __construct(InvoiceRepository $invoiceRepository, CompanyRepository $companyRepository)
    {
        $this->invoiceRepository = $invoiceRepository;
        $this->companyRepository= $companyRepository;
    }

    /**
     * @param int $id
     * @return Company|null
     * @throws CompanyNotFoundException
     */
    public function findCompanyById(int $id): ?Company
    {
        $company = $this->companyRepository->find($id);
        if(is_null($company)){
            throw new CompanyNotFoundException('Company with id: '.$id. ' is not found');
        }
        return $company;

    }

    /**
     * @param CompanyModel $company
     * @return int|null
     */
    public function addCompany(CompanyModel $companyModel): ?int
    {

        $company = new Company();
        $company->setName($companyModel->getName())
            ->setRepresentativeName($companyModel->getRepresentativeName())
            ->setAdress($companyModel->getAdress())
            ->setPhoneNumber($companyModel->getPhoneNumber())
            ->setType($companyModel->getType());
        return $this->companyRepository->add($company, true);

    }

}