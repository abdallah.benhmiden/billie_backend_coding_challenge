<?php

namespace Infrastructure\Model;

class CompanyModel
{
private string $name;
private string $representativeName;
private string $adress;
private string $phoneNumber;
private string $type;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return CompanyModel
     */
    public function setName(string $name): CompanyModel
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getRepresentativeName(): string
    {
        return $this->representativeName;
    }

    /**
     * @param string $representativeName
     * @return CompanyModel
     */
    public function setRepresentativeName(string $representativeName): CompanyModel
    {
        $this->representativeName = $representativeName;
        return $this;
    }

    /**
     * @return string
     */
    public function getAdress(): string
    {
        return $this->adress;
    }

    /**
     * @param string $adress
     * @return CompanyModel
     */
    public function setAdress(string $adress): CompanyModel
    {
        $this->adress = $adress;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     * @return CompanyModel
     */
    public function setPhoneNumber(string $phoneNumber): CompanyModel
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return CompanyModel
     */
    public function setType(string $type): CompanyModel
    {
        $this->type = $type;
        return $this;
    }

}