<?php

namespace Infrastructure\Model;

class InvoiceModel
{
    private float $amount;
    private int $quantity;
    private int $buyerId;
    private int $sellerId;
    private string $serviceName;
    private bool $paid;

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return InvoiceModel
     */
    public function setAmount(float $amount): InvoiceModel
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return InvoiceModel
     */
    public function setQuantity(int $quantity): InvoiceModel
    {
        $this->quantity = $quantity;
        return $this;
    }

    /**
     * @return int
     */
    public function getBuyerId(): int
    {
        return $this->buyerId;
    }

    /**
     * @param int $buyerId
     * @return InvoiceModel
     */
    public function setBuyerId(int $buyerId): InvoiceModel
    {
        $this->buyerId = $buyerId;
        return $this;
    }

    /**
     * @return int
     */
    public function getSellerId(): int
    {
        return $this->sellerId;
    }

    /**
     * @param int $sellerId
     * @return InvoiceModel
     */
    public function setSellerId(int $sellerId): InvoiceModel
    {
        $this->sellerId = $sellerId;
        return $this;
    }

    /**
     * @return string
     */
    public function getServiceName(): string
    {
        return $this->serviceName;
    }

    /**
     * @param string $serviceName
     * @return InvoiceModel
     */
    public function setServiceName(string $serviceName): InvoiceModel
    {
        $this->serviceName = $serviceName;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPaid(): bool
    {
        return $this->paid;
    }

    /**
     * @param bool $paid
     * @return InvoiceModel
     */
    public function setPaid(bool $paid): InvoiceModel
    {
        $this->paid = $paid;
        return $this;
    }

}