<?php

namespace Domain\Common\Presenter;

use Domain\Common\Contracts\ErrorResponseInterface;
use Domain\Common\Contracts\PresenterInterface;
use Domain\Common\Contracts\ResponseInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class Presenter implements PresenterInterface
{

    private SerializerInterface $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function present(ResponseInterface $response)
    {
        return new JsonResponse(json_encode(['ID'=>$response->getIdInvoice()]),Response::HTTP_CREATED, [],true);

    }

    public function presentError(ErrorResponseInterface $errorResponse)
    {
        $content = json_encode(array(
                'code'    => $errorResponse->getCode(),
                'message' => $errorResponse->getMessage()
            )
        );
        return new JsonResponse($content, $errorResponse->getCode(), [],true);
    }
}