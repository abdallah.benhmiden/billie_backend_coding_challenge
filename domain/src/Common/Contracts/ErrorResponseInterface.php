<?php

namespace Domain\Common\Contracts;

interface ErrorResponseInterface
{
    public function getMessage():string;
}