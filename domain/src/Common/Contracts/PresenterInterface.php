<?php

namespace Domain\Common\Contracts;

interface PresenterInterface
{
    public function present(ResponseInterface $response);
    public function presentError(ErrorResponseInterface $response);

}