<?php

namespace Domain\Common\Contracts;

interface ResponseInterface
{
    public function getId(): int;
}