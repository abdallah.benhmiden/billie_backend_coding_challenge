<?php

namespace Domain\Common\Exception;

use Symfony\Component\HttpFoundation\Response;

class CompanyNotFoundException  extends \Exception
{

    /**
     * CompanyNotFoundException constructor.
     * @param string $message
     * @param int $code
     */
    public function __construct(string $message, int $code = Response::HTTP_NOT_FOUND)
    {
        parent::__construct($message, $code);
    }

}