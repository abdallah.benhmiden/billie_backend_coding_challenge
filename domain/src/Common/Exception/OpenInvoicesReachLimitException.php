<?php

namespace Domain\Common\Exception;

use Symfony\Component\HttpFoundation\Response;

class OpenInvoicesReachLimitException  extends \Exception
{

    /**
     * CompanyNotFoundException constructor.
     * @param string $message
     * @param int $code
     */
    public function __construct(string $message, int $code = Response::HTTP_BAD_REQUEST)
    {
        parent::__construct($message, $code);
    }

}