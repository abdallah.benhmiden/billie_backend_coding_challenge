<?php

namespace Domain\Invoice\Create;

use Domain\Common\Contracts\ResponseInterface;

class CreateResponse implements ResponseInterface
{
    private int $invoiceId;

    /**
     * @param int $invoiceId
     */
    public function __construct(int $invoiceId)
    {
        $this->invoiceId = $invoiceId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->invoiceId;
    }

    /**
     * @param int $invoiceId
     * @return CreateResponse
     */
    public function setInvoiceId(int $invoiceId): CreateResponse
    {
        $this->invoiceId = $invoiceId;
        return $this;
    }


}