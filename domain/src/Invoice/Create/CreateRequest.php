<?php

namespace Domain\Invoice\Create;

use Domain\Common\Contracts\RequestInterface;
use Infrastructure\Model\InvoiceModel;

class CreateRequest implements RequestInterface
{
    private InvoiceModel $invoiceModel;

    /**
     * @param InvoiceModel $invoiceModel
     */
    public function __construct(InvoiceModel $invoiceModel)
    {
        $this->invoiceModel = $invoiceModel;
    }

    /**
     * @return InvoiceModel
     */
    public function getInvoiceModel(): InvoiceModel
    {
        return $this->invoiceModel;
    }

    /**
     * @param InvoiceModel $invoiceModel
     * @return CreateRequest
     */
    public function setInvoiceModel(InvoiceModel $invoiceModel): CreateRequest
    {
        $this->invoiceModel = $invoiceModel;
        return $this;
    }


}