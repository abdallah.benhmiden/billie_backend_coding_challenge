<?php

namespace Domain\Invoice\Create;

use Domain\Common\Contracts\ErrorResponseInterface;

class CreateErrorResponse implements ErrorResponseInterface
{
    private string $message;
    private int $code;

    /**
     * @param string $message
     * @param int $code
     */
    public function __construct(string $message, int $code)
    {
        $this->message = $message;
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return CreateErrorResponse
     */
    public function setMessage(string $message): CreateErrorResponse
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return CreateErrorResponse
     */
    public function setCode(int $code): CreateErrorResponse
    {
        $this->code = $code;
        return $this;
    }




}