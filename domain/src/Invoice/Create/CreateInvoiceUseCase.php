<?php

namespace Domain\Invoice\Create;

use Domain\Common\Contracts\RequestInterface;
use Domain\Common\Exception\CompanyNotFoundException;
use Domain\Common\Exception\OpenInvoicesReachLimitException;
use Domain\Invoice\Contracts\CompanyGatewayInterface;
use Domain\Invoice\Contracts\CreateInvoiceUseCaseInterface;
use Domain\Invoice\Contracts\InvoiceGatewayInterface;

class CreateInvoiceUseCase implements CreateInvoiceUseCaseInterface
{

    private InvoiceGatewayInterface $invoiceGateway;
    private CompanyGatewayInterface $companyGateway;
    const LIMIT_INVOICE = 5;

    public function __construct(InvoiceGatewayInterface $invoiceGateway, CompanyGatewayInterface $companyGateway)
    {
        $this->invoiceGateway = $invoiceGateway;
        $this->companyGateway = $companyGateway;
    }

    public function handleRequest(RequestInterface $request): ?int
    {

        $invoiceModel = $request->getInvoiceModel();
        $buyer = $this->companyGateway->findCompanyById($invoiceModel->getBuyerId());
        // check if buyer and seller exist, else throw exception
        if (is_null($buyer)) {
            throw new CompanyNotFoundException('Buyer with id ' . $invoiceModel->getBuyerId() . ' is not found');
        }
        $seller = $this->companyGateway->findCompanyById($invoiceModel->getSellerId());
        if (is_null($seller)) {
            throw new CompanyNotFoundException('Seller with id ' . $invoiceModel->getSellerId() . ' is not found');
        }

        // if count >limit throw exception
        $numberOfBuyerInvoices = $this->invoiceGateway->countInvoiceByCompany($buyer);
        if($numberOfBuyerInvoices > self::LIMIT_INVOICE) {
            throw new OpenInvoicesReachLimitException('Company '. $buyer->getName(). ' has already '.$numberOfBuyerInvoices . ' open invoices');

        }
        return $this->invoiceGateway->addInvoice($invoiceModel, $buyer, $seller);

    }
}