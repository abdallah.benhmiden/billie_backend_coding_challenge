<?php

namespace Domain\Invoice\Contracts;


use Domain\Common\Contracts\RequestInterface;

interface AddCompanyUseCaseInterface
{
    public function handleRequest(RequestInterface $request): ?int;
}