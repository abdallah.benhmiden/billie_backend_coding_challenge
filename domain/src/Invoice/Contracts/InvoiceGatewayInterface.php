<?php

namespace Domain\Invoice\Contracts;

use Infrastructure\Model\InvoiceModel;
use Infrastructure\Persistence\Entity\Company;
use Infrastructure\Persistence\Entity\Invoice;

interface InvoiceGatewayInterface
{
    public  function findInvoiceById(int $id): ?Invoice;
    public  function addInvoice(InvoiceModel $invoice, Company $buyer, Company $seller): ?int;
    public function countInvoiceByCompany(Company $company): int;

}