<?php

namespace Domain\Invoice\Contracts;

use Infrastructure\Model\CompanyModel;
use Infrastructure\Persistence\Entity\Company;

interface CompanyGatewayInterface
{
    public  function findCompanyById(int $id): ?Company;
    public  function addCompany(CompanyModel $invoice): ?int;

}