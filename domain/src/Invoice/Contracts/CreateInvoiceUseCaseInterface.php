<?php

namespace Domain\Invoice\Contracts;


use Domain\Common\Contracts\RequestInterface;

interface CreateInvoiceUseCaseInterface
{
    public function handleRequest(RequestInterface $request): ?int;
}