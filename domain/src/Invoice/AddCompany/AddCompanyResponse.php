<?php

namespace Domain\Invoice\AddCompany;

use Domain\Common\Contracts\ResponseInterface;

class AddCompanyResponse implements ResponseInterface
{
    private int $companyId;

    /**
     * @param int $companyId
     */
    public function __construct(int $companyId)
    {
        $this->companyId = $companyId;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     * @return AddCompanyResponse
     */
    public function setCompanyId(int $companyId): AddCompanyResponse
    {
        $this->companyId = $companyId;
        return $this;
    }

    /**
     * @param int $companyId
     */



}