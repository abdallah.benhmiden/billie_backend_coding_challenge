<?php

namespace Domain\Invoice\AddCompany;

use Domain\Common\Contracts\ErrorResponseInterface;

class AddCompanyErrorResponse implements ErrorResponseInterface
{
    private string $message;
    private int $code;

    /**
     * @param string $message
     * @param int $code
     */
    public function __construct(string $message, int $code)
    {
        $this->message = $message;
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     * @return AddCompanyErrorResponse
     */
    public function setMessage(string $message): AddCompanyErrorResponse
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @param int $code
     * @return AddCompanyErrorResponse
     */
    public function setCode(int $code): AddCompanyErrorResponse
    {
        $this->code = $code;
        return $this;
    }




}