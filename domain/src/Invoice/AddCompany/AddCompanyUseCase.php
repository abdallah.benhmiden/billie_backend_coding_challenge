<?php

namespace Domain\Invoice\AddCompany;

use Domain\Common\Contracts\RequestInterface;
use Domain\Invoice\Contracts\CompanyGatewayInterface;
use Domain\Invoice\Contracts\AddCompanyUseCaseInterface;

class AddCompanyUseCase implements AddCompanyUseCaseInterface
{

    private CompanyGatewayInterface $companyGateway;

    public function __construct(CompanyGatewayInterface $companyGateway)
    {
        $this->companyGateway = $companyGateway;
    }

    public function handleRequest(RequestInterface $request): ?int
    {

        $companyModel = $request->getCompanyModel();
        return  $this->companyGateway->addCompany($companyModel);

    }
}