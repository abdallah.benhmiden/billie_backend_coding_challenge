<?php

namespace Domain\Invoice\AddCompany;

use Domain\Common\Contracts\RequestInterface;
use Infrastructure\Model\CompanyModel;

class AddCompanyRequest implements RequestInterface
{
    private CompanyModel $companyModel;

    /**
     * @param CompanyModel $companyModel
     */
    public function __construct(CompanyModel $companyModel)
    {
        $this->companyModel = $companyModel;
    }

    /**
     * @return CompanyModel
     */
    public function getCompanyModel(): CompanyModel
    {
        return $this->companyModel;
    }

    /**
     * @param CompanyModel $companyModel
     * @return AddCompanyRequest
     */
    public function setCompanyModel(CompanyModel $companyModel): AddCompanyRequest
    {
        $this->companyModel = $companyModel;
        return $this;
    }




}