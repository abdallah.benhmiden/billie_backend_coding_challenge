<?php

namespace Domain\Tests\Create;

use Domain\Invoice\AddCompany\AddCompanyRequest;
use Domain\Invoice\AddCompany\AddCompanyUseCase;
use Domain\Invoice\Contracts\CompanyGatewayInterface;
use Domain\Tests\Utils;
use Infrastructure\Persistence\Entity\Company;
use PHPUnit\Framework\TestCase;

class AddCompanyUseCaseTest extends TestCase
{
    use Utils;
    private $prophet;

    /**
     * @test
     * @throws \Domain\Common\Exception\CompanyNotFoundException
     */
    public function it_will_create_new_company()
    {
        $companyModel = $this->getCompanyModel();
        $addCompanyRequest = new AddCompanyRequest($companyModel);
        $companyGateway = $this->prophet->prophesize(CompanyGatewayInterface::class);
        $addCompanyUseCase = new AddCompanyUseCase($companyGateway->reveal());
        $company = new Company();
        $company->setName('TestCompanyBuyer')->setPhoneNumber('+49 30 78999266')
                ->setAdress('adress')->setRepresentativeName('test')->setType('Type');
        $companyGateway->addCompany($companyModel)->willReturn(1);
        $id = $addCompanyUseCase->handleRequest($addCompanyRequest);
        $this->assertEquals(1, $id);

    }

    protected function setUp() :void
    {
        $this->prophet = new \Prophecy\Prophet;
    }

    protected function tearDown() :void
    {
        $this->prophet->checkPredictions();
    }

}