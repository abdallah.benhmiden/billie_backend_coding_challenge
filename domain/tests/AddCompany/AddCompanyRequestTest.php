<?php

namespace Domain\Tests\AddCompany;

use Domain\Invoice\AddCompany\AddCompanyRequest;
use Domain\Tests\Utils;
use Infrastructure\Model\CompanyModel;
use PHPUnit\Framework\TestCase;

class AddCompanyRequestTest  extends TestCase
{
    use Utils;

    /**
     * @test
     */
    public function it_will_return_true_when_request_is_valid()
    {
        $companyModel = $this->getCompanyModel();
        $addCompanyRequest = new AddCompanyRequest($companyModel);
        $this->assertInstanceOf( CompanyModel::class, $addCompanyRequest->getCompanyModel());
    }
    /**
     * @test
     */
    public function it_will_return_true_when_paid_is_boolean()
    {
        $companyModel = $this->getCompanyModel();
        $addCompanyRequest = new AddCompanyRequest($companyModel);
        $this->assertEquals('Buisness Company GMBH', $addCompanyRequest->getCompanyModel()->getName());
    }
}