<?php

namespace Domain\Tests\Create;


use Domain\Invoice\AddCompany\AddCompanyRequest;
use Domain\Invoice\AddCompany\AddCompanyResponse;
use Domain\Tests\Utils;
use PHPUnit\Framework\TestCase;

class AddCompanyResponseTest  extends TestCase
{
    use Utils;

    /**
     * @test
     */
    public function it_will_return_true_when_response_is_valid()
    {
        $addCompanyResponse = new AddCompanyResponse(1);
        $this->assertEquals(1, $addCompanyResponse->getId());
    }
    /**
     * @test
     */
    public function it_will_return_true_when_type_is_Creditor()
    {
        $companyModel = $this->getCompanyModel();
        $addCompanyRequest = new AddCompanyRequest($companyModel);
        $this->assertEquals('Creditor', $addCompanyRequest->getCompanyModel()->getType());
    }

}