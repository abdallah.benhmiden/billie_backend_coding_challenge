<?php

namespace Domain\Tests\Create;

use Domain\Invoice\Create\CreateRequest;
use Domain\Invoice\Create\CreateResponse;
use Domain\Tests\Utils;
use PHPUnit\Framework\TestCase;

class CreateResponseTest  extends TestCase
{
    use Utils;

    /**
     * @test
     */
    public function it_will_return_true_when_response_is_valid()
    {
        $createResponse = new CreateResponse(1);
        $this->assertEquals(1,$createResponse->getId());
    }
    /**
     * @test
     */
    public function it_will_return_true_when_paid_is_boolean()
    {
        $invoiceModel = $this->getInvoiceModel();
        $createRequest = new CreateRequest($invoiceModel);
        $this->assertIsBool( $createRequest->getInvoiceModel()->isPaid());
    }
    /**
     * @test
     */
    public function it_will_return_true_when_quantity_is_valid()
    {
        $invoiceModel = $this->getInvoiceModel();
        $createRequest = new CreateRequest($invoiceModel);
        $this->assertEquals(20, $createRequest->getInvoiceModel()->getQuantity());
        }

}