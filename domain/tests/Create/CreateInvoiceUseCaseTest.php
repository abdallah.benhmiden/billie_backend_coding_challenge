<?php

namespace Domain\Tests\Create;

use Domain\Common\Exception\CompanyNotFoundException;
use Domain\Common\Exception\OpenInvoicesReachLimitException;
use Domain\Invoice\Contracts\CompanyGatewayInterface;
use Domain\Invoice\Contracts\InvoiceGatewayInterface;
use Domain\Invoice\Create\CreateInvoiceUseCase;
use Domain\Invoice\Create\CreateRequest;
use Domain\Tests\Utils;
use Infrastructure\Persistence\Entity\Company;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;

class CreateInvoiceUseCaseTest extends TestCase
{
    use Utils;
    private $prophet;

    /**
     * @test
     * @throws \Domain\Common\Exception\CompanyNotFoundException
     */
    public function it_will_create_new_invoice()
    {
        $invoiceModel = $this->getInvoiceModel();
        $createRequest = new CreateRequest($invoiceModel);
        $invoiceGateway = $this->prophet->prophesize(InvoiceGatewayInterface::class);
        $companyGateway = $this->prophet->prophesize(CompanyGatewayInterface::class);
        $createInvoiceUseCase = new CreateInvoiceUseCase($invoiceGateway->reveal(), $companyGateway->reveal());
        $company = new Company();
        $company->setName('TestCompanyBuyer')->setPhoneNumber('+49 30 78999266')
                ->setAdress('adress')->setRepresentativeName('test')->setType('Type');
        $companyGateway->findCompanyById(Argument::any())->willReturn($company);
        $invoiceGateway->addInvoice($invoiceModel, $company, $company )->willReturn(1);
        $invoiceGateway->countInvoiceByCompany(Argument::any())->willReturn(2);
        $id = $createInvoiceUseCase->handleRequest($createRequest);
        $this->assertEquals(1, $id);

    }
    /**
     * @test
     * @throws \Domain\Common\Exception\CompanyNotFoundException
     */
    public function it_will_throw_an_exception_when_limit_is_reached()
    {
        $invoiceModel = $this->getInvoiceModel();
        $createRequest = new CreateRequest($invoiceModel);
        $invoiceGateway = $this->prophet->prophesize(InvoiceGatewayInterface::class);
        $companyGateway = $this->prophet->prophesize(CompanyGatewayInterface::class);
        $createInvoiceUseCase = new CreateInvoiceUseCase($invoiceGateway->reveal(), $companyGateway->reveal());
        $company = new Company();
        $company->setName('TestCompanyBuyer')->setPhoneNumber('+49 30 78999266')
            ->setAdress('adress')->setRepresentativeName('test')->setType('Type');
        $companyGateway->findCompanyById(Argument::any())->willReturn($company);
        $invoiceGateway->addInvoice($invoiceModel, $company, $company )->willReturn(1);
        $invoiceGateway->countInvoiceByCompany(Argument::any())->willReturn(6);
        $this->expectException(OpenInvoicesReachLimitException::class);
        $id = $createInvoiceUseCase->handleRequest($createRequest);

    }
    /**
     * @test
     * @throws \Domain\Common\Exception\CompanyNotFoundException
     */
    public function it_will_throw_an_exception_when_buyer_is_not_found()
    {
        $invoiceModel = $this->getInvoiceModel();
        $createRequest = new CreateRequest($invoiceModel);
        $invoiceGateway = $this->prophet->prophesize(InvoiceGatewayInterface::class);
        $companyGateway = $this->prophet->prophesize(CompanyGatewayInterface::class);
        $createInvoiceUseCase = new CreateInvoiceUseCase($invoiceGateway->reveal(), $companyGateway->reveal());
        $company = null;
        $companyGateway->findCompanyById(Argument::any())->willReturn($company);
        $this->expectException(CompanyNotFoundException::class);
        $id = $createInvoiceUseCase->handleRequest($createRequest);


    }

    protected function setUp() :void
    {
        $this->prophet = new \Prophecy\Prophet;
    }

    protected function tearDown() :void
    {
        $this->prophet->checkPredictions();
    }

}