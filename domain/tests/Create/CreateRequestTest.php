<?php

namespace Domain\Tests\Create;

use Domain\Invoice\Create\CreateRequest;
use Domain\Tests\Utils;
use Infrastructure\Model\InvoiceModel;
use PHPUnit\Framework\TestCase;

class CreateRequestTest  extends TestCase
{
    use Utils;

    /**
     * @test
     */
    public function it_will_return_true_when_request_is_valid()
    {
        $invoiceModel = $this->getInvoiceModel();
        $createRequest = new CreateRequest($invoiceModel);
        $this->assertInstanceOf( InvoiceModel::class, $createRequest->getInvoiceModel());
    }
    /**
     * @test
     */
    public function it_will_return_true_when_paid_is_boolean()
    {
        $invoiceModel = $this->getInvoiceModel();
        $createRequest = new CreateRequest($invoiceModel);
        $this->assertIsBool( $createRequest->getInvoiceModel()->isPaid());
    }
    /**
     * @test
     */
    public function it_will_return_true_when_quantity_is_valid()
    {
        $invoiceModel = $this->getInvoiceModel();
        $createRequest = new CreateRequest($invoiceModel);
        $this->assertEquals(20, $createRequest->getInvoiceModel()->getQuantity());
        }

}