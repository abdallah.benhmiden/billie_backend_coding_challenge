<?php

namespace Domain\Tests;

use Infrastructure\Model\CompanyModel;
use Infrastructure\Model\InvoiceModel;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

trait Utils
{
    /**
     * @param $json
     * @param $className
     * @return array|object
     */
    public function deserialize($json, $className)
    {
        $serializer = new Serializer(
            [
                new DateTimeNormalizer(),
                new ObjectNormalizer(
                    null,
                    null,
                    null,
                    new ReflectionExtractor
                ),
                new GetSetMethodNormalizer(),
                new ArrayDenormalizer()
            ],
            [new JsonEncoder()]
        );
        return $serializer->deserialize($json, $className, 'json');
    }

    /**
     * @return array|InvoiceModel|object
     */
    private function getInvoiceModel()
    {
        $fakeRequestContent = <<<EOF
       {
        "amount": 30000,
        "quantity":20,
        "buyerId":1,
        "sellerId": 2,
        "serviceName":"Cars",
        "paid":false
    }
EOF;
        return $this->deserialize($fakeRequestContent, 'Infrastructure\Model\InvoiceModel');
    }

    /**
     * @return array|CompanyModel|object
     */
    private function getCompanyModel()
    {
        $fakeRequestContent = <<<EOF
       {
        "name": "Buisness Company GMBH",
        "representativeName":"Buisness Company GMBH",
        "adress":"Neue Schönhauser Str. 3-5, 10178 Berlin, Allemagne",
        "phoneNumber": "+49 30 78999266",
        "type":"Creditor"

}
EOF;
        return $this->deserialize($fakeRequestContent, 'Infrastructure\Model\CompanyModel');
    }

}
