
# 1. Start the container
Download and launch the container. Especially the download process may take a while if your internet connection is slower.
```bash
❯ docker-compose up -d
```

Run `docker ps` to find out the name of your php container.
```bash
❯ docker ps
CONTAINER ID   IMAGE                                    COMMAND                  CREATED              STATUS                        PORTS                                                        NAMES
215a373c648e   billie_backend_coding_challenge_apache   "/docker-entrypoint.…"   About a minute ago   Up About a minute             443/tcp, 0.0.0.0:9090->80/tcp, :::9090->80/tcp              billie_backend_coding_challenge_apache_1
07bb31887989   billie_backend_coding_challenge_php      "docker-php-entrypoi…"   About a minute ago   Up About a minute             9000/tcp                                                    billie_backend_coding_challenge_php_1
1f89babf7584   mysql:8.0.19                             "/entrypoint.sh mysq…"   About a minute ago   Up About a minute (healthy)   0.0.0.0:3306->3306/tcp, :::3306->3306/tcp, 33060/tcp        billie_backend_coding_challenge_mysql_1
419ac7a4fe5c   phpmyadmin/phpmyadmin:5.0.1              "/docker-entrypoint.…"   About a minute ago   Up About a minute ago         0.0.0.0:9091->80/tcp, :::9091->80/tcp                       phpmyadmin
```
then you can enter the php container:
```bash
❯ docker exec -it billie_backend_coding_challenge_php_1 /bin/ash
```

# In the container: Initialization
```bash
❯ composer install
❯ php bin/console doctrine:schema:update --force
```

# Congratulations !
You have successfully set up your DEV environment. You can now access the application in your browser
```bash
http://127.0.0.1:9090/status
```
You can now access the database in your browser with phpmyadmin
```bash
http://127.0.0.1:9091/
```
To facilitate execution and calling of APIs, a postman collection Billie.postman_collection.json is attached with the project files.
After importing the Postman collection, the Requests are added to a Billie folder

README.md
# Troubleshooting

## docker container are not starting
please shutdown all other projects in docker to avoid using port 80 and 3306. You can run `docker ps` to identify other running containers. Easiest way is to stop then via UI

#### Already used port 3306 for mysql
when you can't temporary stop the container, or you have a mysql running directly on your machine, please change the port in the following files
- In file:`./docker-compose.yml`
    - Change the port 3306 to 3307 in lines 19 and 35
- In file: `./.env`
    - Change the port 3306 to 3307 in line 34 (DATABASE_URL)
- Run `docker-compose up -d` to update the container settings
